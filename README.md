# OSHW-System-On-Module

## Abstraction

Fabrication of Printed Circuit Boards ("PCBs") is not very economical as the cost of fabrication raises (among others) with size and the amount of layers and addition to that it also often requires to sign an NDA (such as with NXP) and/or restriction on release of important documents (documentation, reference schematics and gerbers) and often the sources for the product iself (e.g. [OLIMEX RK3328-SOM-1G](https://www.olimex.com/Products/SOM/RK3328/RK3328-SOM-1G))

Which is a limiting factor for Open-Source Hardware (OSHW) development that I want to address.

## Idea

The inspiration comes from OLIMEX's SODIMM-204 project which is a solution of two PCBs:

**Module Board**

Board with 4~12+ layers of restricted size that includes the chip, RAM and optionally chip-specific components such as the [OLIMEX A20-SOM204](https://www.olimex.com/Products/SOM204/A20/A20-SOM204):

![image](https://git.dotya.ml/attachments/612f40b3-91d0-48e3-bb4d-d6cfc680e335)

Notice that it has an edge connector to fit in SODIM-204 connector.

For clarity it's this connector, often used for RAM sticks in notebooks:

![image](https://git.dotya.ml/attachments/5281a7b2-4c1a-4511-af1b-db5b8706a04a)

**Platform Board (Evaluation Board)**

Board with SODIMM-204 connector that the Module Board slots in such as the [OLIMEX A20-SOM204-EVB](https://www.olimex.com/Products/SOM204/A20-SOM204-EVB) which then adds various devices (Ethernet, PCIe, GPIO, Battery, HDMI, etc..) 

![image](https://git.dotya.ml/attachments/5b7e32aa-e58e-453c-8195-7baa3bce57bb)

which declares the standard for the DDR3 SODIMM 204 pin connector as:

![image](https://git.dotya.ml/attachments/5208894f-6201-4870-86be-59ace8b0ab7a)

that can then be used to design anything from drones, tablets, cluster servers, etc.. on a platform board PCB that is 1~4 layers and costs around 25 EUR to fabricate.

This effectically allows us to make variety of OSHW projects very economically that use hardware comparable to those in raspberry pi and beyond which integrates on difficulty of changing the RAM in a common notebook.

### Use of proprietary module boards

Additionally many chips are near impossible to get as OSHW-compatible due to consumer-hostile business plan and strategy until the chip reaches it's peak economical cycle and becomes obsolete (some not even after obsolescence such as Apple, Inc.).

In order to use more resource efficient hardware this would allow the module boards to be produced in batches of 1000+ to keep the cost economical while significantly limiting tha bad impact from proprietary components on hackability and flexibility of the OSHW project (though the security and privacy concerns will remain) as the user will still be able to adjust the platform board to fit their requirements.

## Issue

204 pins is not enough which eventually lead to OLIMEX using non-standardized headers per chip that break portability such as the [OLIMEX A20-SOM-EVB](https://www.olimex.com/Products/SOM/A20/A20-SOM-EVB/open-source-hardware) Using 240 pins.

The obvious solution would be to use DDR5 SODIMM which has 262pins, but that still feels like using the wrong tool for the job -> Is there a better connecting solution for this usecase?

## Projected cost, fabrication, development and usage of OSHW projects utilizing this standard

This standard is a blocker issue for kreyren's (author of this paper) OSHW tablet design for which the abstracted cost and fabrication process is:
1. Downloading the libre KiCAD designs for the "platform board" and send them them to local PCB fabricator (~25 EUR for 5 PCBs)
2. Buying the "Module Board" - For reference I bought OLIMEX's A20-SOM (https://www.olimex.com/Products/SOM/A20/A20-SOM) locally for 13 EUR/USD including shipping, first hand costs 35 EUR/USD
3. Download the chasis implemented in OpenSCAD/LibFive (projected to be generatable depending on what devices and configuration the user wants) and put that on a 3D printer (optionally using pre-optimized print to skip the slicer phase and get reproducible results (for technical materials as the chasis is designed to be printed using a PolyPropylen + GF30 which is very difficult to print)
4. Source additional components such as touch screen display, etc.. - Projected to have a list of compatible monitors and mediawiki for people to add where these can be bought locally..
* I can get a 10" touch screen display for <5 EUR from a e-recycling center which usually has a lot of them (and the chasis is functional design that can be generated to fit the display), ~30 EUR from second hand and 30-80 EUR online e.g. OLIMEX sells one for 78 EUR https://www.olimex.com/Products/OLinuXino/LCD/LCD-OLinuXino-10/open-source-hardware
* Battery seems to be around 15 EUR and is projected to be lower through OSHW battery design and BMS.
* No other components are abstracted to be needed in bare minimal configuration
5. Put it all together

To get fully libre tablet for the cost of :
* ~40 EUR in PCBs
* cost of the components - 20 EUR ~ 100 EUR
* 3D print - Assuming that people have access to a 3D printer or makerspace where they can use one (I have access to the 3D printer for free sponsored by city of Brno to all citizens and local makerspace costs ~5 EUR to use for a day outside of membership or asking makerspace member to let you in and print it for them for the cost of fillament), worst case paying for 3D print which is usually fee of ~10 EUR

That you can adjust to any kind of computing you want for ~25 EUR for new platform PCB with community support over e.g. Matrix

In terms of software if the chip has a linux mainline support then the path of least resistance seems to be using the installer provided by your preferred linux distribution or declaring a configuration file for armbian, example of one for OLIMEX Teres-A64 maintained by kreyren (author of this paper) is https://github.com/armbian/build/blob/main/config/boards/olimex-teres-a64.conf

The closest comparable project would be [OLIMEX Teres-A64](https://www.olimex.com/Products/DIY-Laptop) which kit costs 240 EUR, the cost of the mainboard from first hand is 80 EUR (https://www.olimex.com/Products/DIY-Laptop/SPARE-PARTS/TERES-PCB1-A64/open-source-hardware) and fabricating board with dimensions 113.5x89mm of 6 layers costs around 68~280 EUR for 5 boards without components.

### More options for PCB Materials

This solution also allows us to use different materials economically for the platform board.

#### FR4

Around 5~20 EUR for 1 or 2 layers

Around 20~70 EUR for 3 or 4 layers

#### Copper Core PCBs

Around 60 EUR if kept at 1 layer

#### Rogers

Around 100 EUR if kept at 2 layers

#### PTFE Teflon

Around 50 EUR if kept at 2 layers

#### Flex

Around 30 EUR for 2 layers

Around 200 EUR for 3 and 4 layers

Could be more economical and ecological than the common FR4 and probably better option for prototyping, but use in production and mission critical environment is discouraged (could be placed on a solid material?)

#### Ridgit flex

Cost and usage unknown

#### Shengul SH260

Cost and usage unknown

## Possible solutions

### Use of Edge Connectors

Edge connectors are set of contact points on the edge of the PCB designed to slot into a connector, for example it's often used on PCIe x16 graphics cards:

![image](https://git.dotya.ml/attachments/54bd7c9b-b9d6-49ac-9965-5e1fb7d6ee55)

Which has a bevelled edge (minor added cost to fabrication) to fit in the connector

![image](https://git.dotya.ml/attachments/f596d7db-ce35-49f9-8584-ff8e0fcc1ad5)

One solution could be to standardize these connectors and invent a symbol that can be put over them signaling that they follow this standard.

Furthermore in practical example this can be adjusted to making Single Board Computers ("SBCs") more functional:

Lets imagine that we have [OLIMEX OlinuXino-A64](https://www.olimex.com/Products/OLinuXino/A64/A64-OLinuXino/open-source-hardware) board as an example:

![image](https://git.dotya.ml/attachments/15b998d5-eaa8-49bb-8a8b-6bf33adf88e6)

which could have an edge connector some-what painlessly added to it

![image](https://git.dotya.ml/attachments/3117b88e-9936-465b-adf7-7b9664da5adb)
 
(note that contacts are on both sides of the PCB)

This would efectivelly enable OSHW manufacturers to adjust their SBCs so that they can be slotted in this solution.

### Mezzanine connector

![](https://connectorsupplier.com/wp-content/uploads/Samtec-SEARAY--scaled.jpg)

### 262-pin DDR5 SO-DIMM Connector

TBD

### Additional Notes

Originally discussed in https://git.dotya.ml/kreyren/kreyren/issues/75

The idea was brainstormed on forum of The Open Source Hardware Association at https://community.oshwa.org/t/standardization-of-oshw-system-on-module-projects/605

### References
1. OLIMEX's SOM204 platform on GitHub -- https://github.com/OLIMEX/SOM204/blob/master/SOM204-EVB/SOM204_EVB_Platform.pdf
2. Explanation to what is an edge connector by eurocircuits -- https://www.eurocircuits.com/pcb-design-guidelines/edge-connectors